function h5_from_l1nb()

%% Find NB figure and axes

% root begets figures
hFigs = findall(0, 'Type', 'Figure');
for n = 1:numel(hFigs)
    if hFigs(n).Number == 1008
        break;
    end
end

% figure begets axes
hAxes = findobj(hFigs(n), 'Type', 'Axes');

%% Extract a date from the figure and convert to ISO UTC date stamp

import java.util.TimeZone;
import java.text.SimpleDateFormat;
import java.util.Date;

utcTimeZone = TimeZone.getTimeZone('UTC');
isoDateFormatter = SimpleDateFormat('yyyy-MM-dd''T''HH:mm:ss''Z''');
isoDateFormatter.setTimeZone(utcTimeZone);

try
    hIDfig = findobj(hAxes, 'Type', 'Text');
    for n = 1:numel(hIDfig)
        txtstr = hIDfig(n).String;
        date_label = 'DARM data from ';
        date_labelloc = strfind(txtstr, date_label);
        if isempty(date_labelloc)
            continue;
        else
            date_str = txtstr(date_labelloc + numel(date_label):end);
            date_unix = GWData.datenum_to_unix(GWData.tzconvert(date_str));
            date_java = Date(1000*date_unix);
            nbDate = char(isoDateFormatter.format(date_java));
            break;
        end
    end
catch
    warning('No date stamp found -- using today''s date instead')
    nbDate = char(isoDateFormatter.format(Date()));
end


%% Extract datasets from the figure, write to hdf5

h5filename = ['L1NB_' nbDate '.hdf5'];

% axes begets lines
hLines = findobj(hAxes, 'Type', 'Line');

% line begets xdata, ydata
% special processing for hLines(1) (measured data):
% extract frequency dataset
dataset = '/frequency';
data = hLines(1).XData;
myh5write(h5filename, dataset, data);
% extract measured dataset
dataset = '/measured';
data = hLines(1).YData;
myh5write(h5filename, dataset, data);

% special processing for hLines(2) (sum data):
% extract sum dataset
dataset = '/sum';
data = hLines(2).YData;
myh5write(h5filename, dataset, data);

% extract remaining datasets
for n = 3:numel(hLines)
    assert(~isempty(hLines(n).DisplayName), ['Trace ' num2str(n) ' lacks a name']);
    dataset = ['/traces/' hLines(n).DisplayName];
    data = hLines(n).YData;
    myh5write(h5filename, dataset, data);
end

%% Write hdf5 attributes for NB schema

h5writeatt(h5filename, '/', 'schema', 'aLIGO Noise Budget');
h5writeatt(h5filename, '/', 'version', int64(1));
h5writeatt(h5filename, '/', 'name', 'L1');
h5writeatt(h5filename, '/', 'date', nbDate);

end

function myh5write(h5filename, h5dataset, data)

h5create(h5filename, h5dataset, size(data));
h5write(h5filename, h5dataset, data);

end
#!/usr/bin/env python2.7

from time import sleep
import numpy as np

from bokeh.client import push_session
from bokeh.plotting import figure, curdoc

from nds2 import connection

c = connection("nds.ligo-la.caltech.edu", 31200)
i = c.iterate(['L1:LSC-DARM_IN1_DQ'])
b = i.next()
data = b[0].data[:10000]

p = figure(title='L1:LSC-DARM_IN1_DQ')
r = p.line(range(len(data)), data, color="navy", line_width=4)


# open a session to keep our local document in sync with server
session = push_session(curdoc(), session_id='test')

def update():
  b = i.next()
  print b
  data = b[0].data[:10000]
  r.data_source.data["y"] = data

curdoc().add_periodic_callback(update, 50)

session.show(p) # open the document in a browser

session.loop_until_closed()

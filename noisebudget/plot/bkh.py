#from bokeh.plotting import figure, output_file, show, save
import bokeh.plotting as plt
from bokeh.models import ColumnDataSource, HoverTool, HBox, VBoxForm
from bokeh.models.widgets import Toggle, Button, CheckboxGroup
from bokeh.io import curdoc

from .const import COLORS
# COLORS = [
#     "#75968f", "#a5bab7", "#c9d9d3", "#e2e2e2", "#dfccce",
#     "#ddb7b1", "#cc7878", "#933b41", "#550b1d"
# ]

def plot(nb, xlim=None, ylim=None, save=None):

    top = 0.9
    right = 0.8

    # prepare some data
    x = [0.1, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0]
    y0 = [i**2 for i in x]
    y1 = [10**i for i in x]
    y2 = [10**(i**2) for i in x]

    hover = HoverTool(
        tooltips=[
            ("trace", "@trace"),
            ("freq", "$x"),
            ("strain", "$y"),
        ]
    )

    TOOLS = ['pan','box_zoom','wheel_zoom',
             'crosshair',
             hover,
             'tap',
             'reset','save',
             ]

    # create a new plot
    fig = plt.Figure(
        plot_width=1000, plot_height=600,
        tools=TOOLS,
        title=nb.title,
        x_axis_type='log',
        y_axis_type='log', y_range=ylim,
        x_axis_label='Frequency [Hz]',
        y_axis_label='ASD of displacement [m/Hz^1/2]',
    )

    # "legend" widgets
    labels = []
    widgets = []

    # add SUM
    label = 'Sum'
    source = plt.ColumnDataSource(data=dict(
        # freq=nb.freq,
        # strain=nb.sum.data,
        trace=[label]*len(nb.freq),
        ))
    fig.line(
        nb.freq, nb.sum.data,
        legend=label,
        source=source,
        line_width=2,
        line_dash='dashed',
        color='black',
        )
    labels += [label]

    # add traces
    for i, trace in enumerate(nb):
        source = plt.ColumnDataSource(data=dict(
            # freq=trace.freq,
            # strain=trace.data,
            trace=[trace.label]*len(trace.freq),
            ))

        r = fig.line(
            trace.freq, trace.data,
            legend=trace.label,
            source=source,

            line_width=2,
            color=COLORS[i],
            line_alpha=0.4,

            # set visual properties for selected glyphs
            # selection_color='red',
            # selection_line_alpha=1.0,

            hover_color=COLORS[i],
            hover_line_alpha=1.0,

            # set visual properties for non-selected glyphs
            # nonselection_fill_alpha=0.8,
            # nonselection_fill_color="blue",
            # nonselection_color='green',
            # nonselection_line_alpha=0.1,
            )

        labels += [label]
        # widget = Toggle(label=trace.label)
        # widget = Button(label=trace.label, type="success")
        # widgets.append(widget)

    cbl = CheckboxGroup(
        labels=labels, active=[0, 1])
    # legend = HBox(VBoxForm(*widgets), width=300)

    #update(None, None, None)

    curdoc().add_root(HBox(fig, width=1100))

    if save:
        # output to static HTML file
        plt.output_file(save)
        # plt.show(p)
        plt.save(fig)

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec


def plot(nb, xlim=None, ylim=None, save=None):

    top = 0.9
    right = 0.8
    
    hh = plt.figure(figsize=(8,8))
    gs = GridSpec(1, 1)
    gs.update(left=0.1, right=0.8, bottom=0.1, top=top)
    #ax = hh.add_subplot(right=0.1, left=0.8, bottom=0.1, top=0.1)
    ax = plt.subplot(gs[0,0])

    for trace in nb:
        # print(trace)
        # print(trace.freq, np.shape(trace.freq))
        # print(trace.data, np.shape(trace.data))
        ax.loglog(trace.freq, trace.data,
                  label=trace.label, alpha=0.8)

    ax.loglog(nb.freq, nb.sum.data,
              'k--', linewidth=2,
              label='Sum')

    hh.suptitle(nb.title)
    ax.set_xlabel('Frequency [Hz]')
    ax.set_ylabel(r'ASD of displacement [m/Hz$^{1/2}$]')

    ax.grid('on')
    ax.axis('tight')

    if xlim:
        ax.set_xlim(xlim)
    if ylim:
        ax.set_ylim(ylim)

    ax.legend(loc=2,
              bbox_transform=plt.gcf().transFigure,
              bbox_to_anchor=(right, top),
              )

    if save:
        plt.save(save)
    else:
        plt.show()

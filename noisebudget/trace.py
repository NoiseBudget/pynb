import os
import logging
import numpy as np
import collections

##################################################

def array_compare(a, b):
    """Return True if frequency arrays match
    
    """
    #return np.allclose(a, b)
    logging.debug('%s %s' % (len(a),len(b)))
    return np.array_equal(a, b)

##################################################

class NBTrace(object):
    label = None
    units = None
    metadata = collections.defaultdict()
    data_root = None

    def __init__(self, data_root=None):
        self.data_root = data_root
        self.__loaded = False

    @property
    def name(self):
        return self.__class__.__name__

    ##########

    def set_data(self, freq, data):
        """Set trace data

        """
        # FIXME: add check for length consistency
        self.__freq = freq
        self.__data = data
        self.__loaded = True
        # f0, df = self.get_freq_data(freq)
        # self.metadata['f0'] = f0
        # self.metadata['df'] = df
        # print(self.f0, self.df, len(self))

    ##########

    @classmethod
    def zeros(cls, freq):
        """Create zero-noise trace from a frequency array.

        """
        out = cls()
        data = np.zeros(np.shape(freq))
        out.set_data(freq, data)
        return out

    @classmethod
    def from_data(cls, freq, data, units=None):
        """Create trace from ASD data and frequency array.

        """
        out = cls()
        out.set_data(freq, data)
        out.units = units
        return out

    ##########
    # user override methods

    def fetch(self, path):
        """fetch data for trace

        Should return (freq, data) tuple where 'data' is
        frequency-domain data at frequencies 'freq'.

        """
        freq, data = np.loadtxt(path, unpack=1)
        return freq, data

    def calibrate(self, data):
        """Calibrate data

        """
        return data
        
    ##########

    def load(self, data_root='.'):
        logging.debug(data_root)
        logging.debug(self.path)
        dpath = os.path.join(data_root, self.path)
        logging.info("loading: %s" % (self.name))
        freq, data = self.fetch(dpath)
        logging.info("calibrating: %s" % (self.name))
        data = self.calibrate(data)
        self.set_data(freq, data)
        # FIXME: is this the best way to make this work for this kind
        # of construction?:
        #   freq = NOISEYNOISE().load().freq
        return self
    
    ##########

    @staticmethod
    def get_freq_data(freq):
        return freq[0], freq[1]-freq[0]

    @property
    def data(self):
        """Calibrated data

        """
        return self.__data

    def __len__(self):
        return len(self.data)

    @property
    def freq(self):
        """frequency array"

        """
        # freq = np.arange(len(self), step=self.df) + self.f0
        # print(self.f0, self.df, len(freq))
        # return freq
        return self.__freq

    @property
    def f0(self):
        """Frequency of first data point

        """
        return self.metadata['f0']

    @property
    def df(self):
        """Frequency step

        """
        return self.metadata['df']

    ##########

    def __compare_metadata(self, other):
        if other.units != self.units:
            raise TypeError("%s units disagree" % self.name)
        if len(other) != len(self):
            raise ValueError("%s data different size" % self.name)
        # if not array_compare(self.freq, other.freq):
        #     raise ValueError("%s different frequency arrays" % self.name)
        # if other.f0 != self.f0:
        #     raise ValueError("spectra do not have same starting frequency")
        # if other.df != self.df:
        #     raise ValueError("spectra do not have same frequency spacing")
        
    def __add__(self, other):
        self.__compare_metadata(other)
        # for t in [self, other]:
        #     print(np.shape(t.data), type(t.data))
        outdata = np.sqrt(np.sum(np.array((self.data, other.data))**2, axis=0))
        out = NBTrace.from_data(self.freq, outdata, units=self.units)
        return out

    __iadd__ = __add__

    ##########

    def interp(self, newfreq, **kwargs):
        """Re-interpolate data at specified frequncies

        """
        if array_compare(self.freq, newfreq):
            data = self.data
            freq = self.freq
        else:
            logging.info("re-interpolating data %s..." % self.name)
            data = np.interp(newfreq, self.freq, self.data, **kwargs)
            freq = newfreq
            self.set_data(freq, data)
        #self.set_data(newdata, newfreq)
        return freq, data
        

class NBTraceGroup(NBTrace):
    pass


class NBDisplacementTrace(NBTrace):
    pass


class NBSensingTrace(NBTrace):
    def trace(self):
        tf = np.abs(1 - DARM_OLTF) / OPT_GAIN * np.abs(1+1j * ff/darm_pole)
        return self.trace * tf

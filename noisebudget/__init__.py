import logging

FORMAT = '%(levelname)s: %(message)s'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)

from .trace import NBTrace
from .budget import NoiseBudget
from .module import local_path

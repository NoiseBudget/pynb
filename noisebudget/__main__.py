#!/usr/bin/env python3.4

import os
import sys
import logging
import argparse

FORMAT = '%(levelname)s: %(message)s'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)

from .module import load_budget_module
# import .plot.bkh as p_bkh
# import .plot.mpl as p_mpl

############################################################

PROG = 'noisebudget'
description = '''Advanced LIGO noise budget plotting.'''
# usage = '''
# '''

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    prog=PROG,
    #usage=usage,
    #epilog=epilog,
    )

parser.add_argument(
    'module', metavar='<path>',
    type=str,
    help="noise budget module",
    )

parser.add_argument(
    '-pb', '--plot-bokeh',
    action='store_true',
    help="plot bokeh",
    )
parser.add_argument(
    '-pm', '--plot-pdf',
    action='store_true',
    help="plot pdf",
    )

############################################################

if __name__ == '__main__':
    import importlib

    args = parser.parse_args()
    print(args)

    #os.chdir(os.path.dirname(args.module))

    
    mod = load_budget_module(args.module)
    # budget = importlib.import_module(os.path.basename(args.module))
    # print(budget)
    # for t in budget:
    #     print(t)

    mod.budget.load()

    if args.plot_bokeh:
        plot.bkh.plot(budget)

    if args.plot_pdf:
        plot.mpl.plot(budget)

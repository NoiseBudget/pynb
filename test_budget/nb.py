import os
import numpy as np

# FIXME: how to specify version
from noisebudget import NBTrace, NoiseBudget, local_path

def get_complex_interp(fname, ff, **kwargs):
    f1, re, im = np.loadtxt(fname, unpack=1)
    re2 = np.interp(ff, f1, re, **kwargs)
    im2 = np.interp(ff, f1, im, **kwargs)
    return re2+1j*im2

darm_oltf_path = 'Data/Calibration/2015-10-15_before_flip_tf.txt'
opt_gain_ref = 3.26e12 # mA/m
darm_pole_ref = 341 # Hz

##################################################

# class DARM(NBTransferFunction):
#     path = 'Data/Calibration/2015-10-15_before_flip_tf.txt'

#     opt_gain = 3.26e12 # mA/m
#     darm_pole = 341 # Hz

#     def load(self):
#         f, re, im = np.loadtxt(local_path(self.path), unpack=1)

#     def interp(self, ff):
#         f, re, im = np.loadtxt(local_path(self.path), unpack=1)
#         oltf = get_complex_interp(f, re, im, ff)
#         return np.abs(1-oltf)/self.opt_gain * np.abs(1+1j*ff/self.darm_pole)

##################################################

class DARK(NBTrace):
    label = 'Dark noise'
    path = 'Data/OMC/dcpd_dark_wh1.txt'

    def fetch(self, path):
        freq, dcpd_a, dcpd_b = np.loadtxt(path, unpack=1)
        data = np.sqrt(dcpd_a**2 + dcpd_b**2)
        # Undo the loop transfer function and optical plant
        darm_oltf_interp = get_complex_interp(darm_oltf_path, freq)
        data *= np.abs(1-darm_oltf_interp)/opt_gain_ref*np.abs(1+1j*freq/darm_pole_ref)
        return freq, data

class QUANTUM(NBTrace):
    label = 'Quantum [23 W]'
    path = 'Data/GwincCurves/quantumNoise_23,0W_24pct.txt'
    scale = 1.1

    def calibrate(self, data):
        return self.scale * data
    
class SEISMIC(NBTrace):
    label = 'Seismic'
    path = 'Data/GwincCurves/seismicNoise.txt'

class NEWTONIAN(NBTrace):
    label = 'Newtonian'
    path = 'Data/GwincCurves/newtonianNoise.txt'

class COAT_BROWNIAN(NBTrace):
    label = 'Coating Brownian'
    path = 'Data/GwincCurves/coatBrownianNoise.txt'

class SUS_THERMAL(NBTrace):
    label = 'Suspension thermal'
    path = 'Data/GwincCurves/susThermalNoise_Sheila.txt'

class GAS(NBTrace):
    label = 'Residual gas'
    path = 'Data/GwincCurves/resGasNoise.txt'

class SFD(NBTrace):
    label = 'Squeezed film damping'
    path = 'Data/SUS/squeeze_film_asd.txt'


##################################################

freq = NEWTONIAN().load().freq #np.loadtxt(local_path('Data/GwincCurves/newtonianNoise.txt'), unpack=1)
budget = NoiseBudget([QUANTUM,
                      SEISMIC,
                      NEWTONIAN,
                      COAT_BROWNIAN,
                      SUS_THERMAL,
                      GAS,
                      SFD,
                      DARK,
                      ],
                     freq=freq,
                     title="H1 noise budget",
                     )

##################################################

if __name__ == '__main__':
    #from noisebudget.plot import plot
    from noisebudget.plot.bkh import plot

    budget.load()
    budget.interp()

    plot(budget, ylim=[1e-25, 1e-7],
         save=local_path('noisebudget.html'),
         )
